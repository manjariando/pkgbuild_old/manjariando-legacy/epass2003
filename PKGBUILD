# Maintainer: tioguda <guda.flaviog@gmail.com>

_pkgname=ePass2003
pkgname=epass2003
date=20210127
pkgver=1.1.21.125
pkgrel=1.3
pkgdesc="EnterSafe PKI Manager"
arch=('x86_64')
url="https://pronova.com.br/epass2003-download"
license=('')
makedepends=('imagemagick')

DLAGENTS=('https::/usr/bin/curl -k -o %o %u')

source=("https://manjariando.gitlab.io/source-packages/epass2003/${_pkgname}-Linux-${date}.zip"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}.png"
        "https://metainfo.manjariando.com.br/${pkgname}/pkimanager-pt-br-2020.lng")
sha256sums=('ce78294e0de895244598c47eea9c97463d71e4eb4eecafe16357b7036befb44a'
            '8abd20d874c3dae23cdcfa7219e953872ed5af55f0953140ba2684b8c6e51ab8'
            '0e23a7f30207952c1bb28880df6ac00b7fc405b15993cd46cbca671322f1ce26'
            'd22b8454c2bd8971d5162d708b81dd02ac5b23639323030e5b106de2b46f0f82')
install=${pkgname}.install

_epass2003_desktop="[Desktop Entry]
Version=1.0
Type=Application
Name=ePass2003 (User)
Comment=EnterSafe Castle SDK For Linux
Comment[pt_BR]=EnterSafe Castle SDK para Linux
Exec=${pkgname}
Icon=${pkgname}
Categories=Utility;
Terminal=false"

_epass2003admin_desktop="[Desktop Entry]
Version=1.0
Type=Application
Name=ePass2003 (Admin)
Comment=EnterSafe Castle SDK For Linux
Comment[pt_BR]=EnterSafe Castle SDK para Linux
Exec=${pkgname}-admin
Icon=${pkgname}
Categories=Utility;
Terminal=false
X-AppStream-Ignore=true"

build() {
    cd "${srcdir}"
    echo -e "$_epass2003_desktop" | tee com.${pkgname}.desktop
    echo -e "$_epass2003admin_desktop" | tee com.${pkgname}_admin.desktop
}

package() {
    depends=('udev' 'jdk8-openjdk')

    cd "${srcdir}/${_pkgname}-Linux-${date}"
    mkdir -p "${pkgdir}/opt/${pkgname}"
    cp -r x86_64/redist/* "${pkgdir}/opt/${pkgname}"

    # Lang
    install -Dm644 "${srcdir}/pkimanager-pt-br-2020.lng" "${pkgdir}/opt/${pkgname}/lang/pkimanager_2020.lng"

    # Symlink
    mkdir -p "${pkgdir}"/usr/{bin,lib}
    ln -s "/opt/${pkgname}/ePassManager_2003" "${pkgdir}/usr/bin/${pkgname}"
    ln -s "/opt/${pkgname}/ePassManagerAdmin_2003" "${pkgdir}/usr/bin/${pkgname}-admin"
    ln -s "/opt/${pkgname}/libcastle.so.1.0.0" "${pkgdir}/usr/lib/libcastle.so"
    ln -s "/opt/${pkgname}/libcastle.so.1.0.0" "${pkgdir}/usr/lib/libcastle.so.1"
    ln -s "/opt/${pkgname}/libcastle.so.1.0.0" "${pkgdir}/usr/lib/libcastle.so.1.0"
    ln -s "/opt/${pkgname}/libcastle.so.1.0.0" "${pkgdir}/usr/lib/libcastle.so.1.0.0"

    
    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}_admin.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}_admin.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    for size in 16 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/${pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
